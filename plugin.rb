# name: discourse_ci
# about: Plugin custom for Courrier International - Expat
# version: 0.1
# authors: Benoît GRUNENBERGER
# url: https://bitbucket.org/lemonde/mod_discourse_ci.git

# stylesheet
register_asset "stylesheets/ci-forum.css.scss"
