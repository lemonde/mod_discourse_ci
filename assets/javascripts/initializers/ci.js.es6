/**
  Apply CI when the app boots
**/

export default {
  name: "apply-ci",
  initialize: function() {


    // Universal Analytics
    if (!document.getElementById('universalAnalytics')) {
      (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        a.id = "universalAnalytics";
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    }

    // Dimension
    var dimensionValue = 'expat';
    ga('set', 'dimension3', dimensionValue);
    ga('create', 'UA-28847272-1', 'auto');
    ga('send', 'pageview');




    // Xiti
    if (!document.getElementById('xtclicks') && !document.getElementById('xtcore')) {

      var xitiCode = `
     <script type="text/javascript">
     <!--
     xtnv=document;xtsd="http://logc2";if(Modernizr.mq('screen and (min-width: 760px)')){xtsite="552994";}else{xtsite="544860";}xtn2="40";xtpage="Forums";xtdi="";xt_multc="&x1=[Home]&x2=&x3=&x4=[]&x5=[]&x6=";xt_an="0";xt_ac="1";xtidmod="Home";xtergo="0";
     // Do not modify below
     if (window.xtparam!=null){window.xtparam+="&ac="+xt_ac+"&an="+xt_an+xt_multc;}
     else{window.xtparam="&ac="+xt_ac+"&an="+xt_an+xt_multc;};
     //-->
     </script>
     <script type="text/javascript" src="http://www.courrierinternational.com/sites/ci_master/themes/ci/js/xtclicks.js" id="xtclicks" async="true"></script>
     <script type="text/javascript" src="http://www.courrierinternational.com/sites/ci_master/themes/ci/js/xtcore.js" id="xtcore" async="true"></script>

     <noscript>
     <img width="1" height="1" alt="" src="http://logc2.xiti.com/hit.xiti?s=552994&s2=40&p=&di=&an=0&ac=1&x1=[Home]&x2=&x3=&x4=[]&x5=[]&x6=" >
     </noscript>`
      $("body").append(xitiCode);
    }


    // Ajout script SmartServer
    if (!document.getElementById('smartadserver_loader')) {
      var SmartServerCode = "";
      SmartServerCode += "<script id=\"smartadserver_loader\" src=\"http://ww690.smartadserver.com/config.js?nwid=690\" async=false></script>";
      SmartServerCode += "<script>var sas = sas || {};sas.cmd = sas.cmd || [];sas.cmd.push(function() {sas.setup({domain:'http://ww690.smartadserver.com',async:true,insequence:true,renderMode:0});});sas.cmd.push(function() {sas.call(\"onecall\", {siteId:75103,pageId:557069,formatId:'21488,21487,21486,21485,21484,21483,21482',target:'',}, {onLoad: function(oL){console.log(\"SmartAdCall Loaded sas_\" + oL.formatId + \" : \" + oL.hasAd);if(oL.hasAd){document.getElementById('sas_'+oL.formatId).className = document.getElementById('sas_'+oL.formatId).className + \" load\"}else{document.getElementById('sas_'+oL.formatId).className = document.getElementById('sas_'+oL.formatId).className + \" empty\"}}});});</script>";
      $("body").append(SmartServerCode);
    }


    // Filtre
    function xitiFilter(string) {
      var stringFiltered = string.replace('\'|"|-| ', '-');
      stringFiltered = stringFiltered.replace('é|è|ê|ë', 'e');
      stringFiltered = stringFiltered.replace('à|â|ä', 'a');

      var reg = new RegExp('[^a-zA-Z0-9_]*', 'gi');
      return stringFiltered.replace(reg, "");
    }

    /*
      while (!!sas) {
      sas.cmd.push(function(){sas.render(21482);}) && sas.cmd.push(function(){sas.render(21486);}) && sas.cmd.push(function(){sas.render(21487);});
      break;
      }
    */

    function afterLoaded() {

      var idAdHeader, idAdSide, idAdFooter = 0;
      var status = true;

      var pageType;
      var xitiChapitre = 'Forums';

      var level = window.location.pathname.split('/');
      var firstLevel = level[1];


      switch (firstLevel) {

        case '':
          pageType = 'home';
          break;

        case 't':
          pageType = 'topic';

          var topicTitle = $("#topic-title div div h1 a.fancy-title").html();
          var topicId = $("#topic").data('topic-id');

          var categoryName = $("a span.badge-category.clear-badge").html();

          if (!topicId || !topicTitle || typeof(xt_med) !== "function") status = false;

          break;

        case 'c':
          pageType = 'category';
          var categoryName = window.location.pathname.split('/').pop();
          var categoryNameFiltered = xitiFilter(categoryName);


          console.log(categoryName + ' ' + categoryNameFiltered);

          break;

        case 'admin':
          pageType = 'admin';
          sas.cmd.push(function() {
            sas.clean();
          });
          break;

        case 'user':
          pageType = 'user';
          sas.cmd.push(function() {
            sas.clean();
          });
          break;

        default:
          pageType = 'other';
          //alert(pageType);
      }

      console.log(pageType);






      var adPageId = 0;

      switch (categoryName) {
        case 'canada':
          adPageId = 631938;
          break;

        case 'suisse':
          adPageId = 631987;
          break;

        case 'etats-unis':
          adPageId = 631962;
          break;

        case 'maroc':
          adPageId = 631993;
          break;

        case 'royaume-uni':
          adPageId = 631968;
          break;

        case 'allemagne':
          adPageId = 631953;
          break;

        case 'bresil':
          adPageId = 631932;
          break;

        case 'cote-ivoire':
          adPageId = 631999;
          break;

        case 'australie':
          adPageId = 631920;
          break;

        case 'emirats-arabe-unis':
          adPageId = 632011;
          break;

        case 'chine-hongkong':
          adPageId = 632017;
          break;

        case 'singapour':
          adPageId = 632023;
          break;

        case 'norvege':
          adPageId = 631981;
          break;

        case 'israel':
          adPageId = 632005;
          break;

        case 'inde':
          adPageId = 632029;
          break;

        case 'espagne':
          adPageId = 631975;
          break;

        default:
          adPageId = 598715;
      }







      // xtpage

      var xtpage = xitiChapitre + '::';

      switch (pageType) {
        case 'home':
          xtpage += 'Acceuil';
          break;

        case 'category':
          xtpage += categoryName;
          break;

        case 'topic':
          xtpage += categoryName + '::' + topicTitle;
          break;

        case 'user':
          xtpage += 'Utilisateur';
          break;

        case 'admin':
          xtpage += 'Administration';
          break;

        default:
          xtpage += 'Autre';
      }



      // Ajout des pubs
      var adHTML = "";

      // Header
      idAdHeader = 21482;
      if (document.getElementsByClassName('adheader').length < 1) {
        adHTML = "";
        adHTML += "<div class=\"sascontainer ad adheader\">";
        adHTML += "<div id=\"sas_" + idAdHeader + "\"></div>";
        adHTML += "</div>";
        $("#main-outlet div.container").before(adHTML);
        //$("div.list-controls").before(code);
      } else {
        document.getElementsByClassName('adheader')[0].removeAttribute('id');
        document.getElementsByClassName('adheader')[0].setAttribute("id", "sas_" + idAdHeader);
      }



      // Side
      idAdSide = 21484;
      if (document.getElementsByClassName('adside').length < 1) {
        adHTML = "";
        adHTML += "<div class=\"sascontainer ad adside\">";
        adHTML += "<div id=\"sas_" + idAdSide + "\"></div>";
        adHTML += "</div>";
        $("section#main").append(adHTML);
      }


      // Footer
      idAdFooter = 21485;
      if (document.getElementsByClassName('adfooter').length < 1) {
        adHTML = "";
        adHTML += "<div class=\"sascontainer ad adfooter\">";
        adHTML += "<div id=\"sas_" + idAdFooter + "\"></div>";
        adHTML += "</div>";
        $("section#main").after(adHTML);
      }


      sas.cmd.push(function() {
        sas.call("onecall", {
          siteId: 75103,
          pageId: adPageId,
          formatId: '21482,21484,21485',
          target: ''
        });
      });





      if (status == false) {
        window.setTimeout(afterLoaded, 500);
        return;
      }


      console.log('Custom:' + pageType + ' , ' + categoryName + ' , ' + topicTitle);
      console.log('SAS:' + adPageId);
      console.log('Xiti:' + xtn2 + ' , ' + xtpage);

      //sas.cmd.push(function(){sas.refresh();});

      //setInterval(function() {sas.cmd.push(function(){sas.refresh();});}, 8000);
      //window.xt_med("F", xtn2, xtpage + "_image" + nextSlide  + "&ac="+xt_ac+"&an="+xt_an+xt_multc, '');
      //ga('send', 'pageview', window.location.pathname + "#image" + nextSlide);

      xtn2 = 40;
      if (document.getElementById('xtclicks') && document.getElementById('xtcore') && status) xt_med("F", xtn2, xtpage);
    }

    // Event executé a chaque changement de page
    require('discourse/lib/page-tracker').default.current().on("change", function(url) {
      afterLoaded();
    });
  }
};
